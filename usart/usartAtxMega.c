#include <usartAtxMega.h>

#define CLK2X         2
#define DREIF         5
#define MPCM          1
#define RXC1          7
#define TXC1          6
#define ASYNCHRONOUS  0b00000000
#define SYNCHRONOUS   0b01000000
#define IFRACOM       0b10000000
#define MSPI          0b11000000
#define RXEN          4
#define TXEN          3
#define ENABLE_RX_INTERRUPT   0b00100000
#define ENABLE_TX_INTERRUPT   0b00001000

#define DISABLED      0b00000000
#define EVEN          0b00100000
#define ODD           0b00110000

#define BIT1S         0b00000000
#define BIT2S         0b00001000

#define BIT5          0b00000000
#define BIT6          0b00000001
#define BIT7          0b00000010
#define BIT8          0b00000011
#define BIT9          0b00000111

// Constantes BSEL, BSCALE y CLK2X
// 9600 Baudios, 2 Mhz, BSEL=12, BSCALE=0, CLK2X=0
#define BAUDCTRL_9600_2MHZ    0b0000000000001100
// 19200 Baudios, 2 Mhz, BSEL=11, BSCALE=-1, CLK2X=0
#define BAUDCTRL_19200_2MHZ   0b1111000000001011
// 28800 Baudios, Mhz, BSEL= , BSCALE= , CLK2X=
// 38400 Baudios, 2 Mhz, BSEL=9, BSCALE=-2, CLK2X=0
#define BAUDCTRL_38400_2MHZ   0b1110000000001001
// 57600 Baudios, 2 Mhz, BSEL=75, BSCALE=-6, CLK2X=0
#define BAUDCTRL_57600_2MHZ   0b1010000001001011

// 9600 Baudios, 4 Mhz, BSEL=3205, BSCALE=-7, CLK2X=0
#define BAUDCTRL_9600_4MHZ    0b1001110010000101
// 19200 Baudios, 4 Mhz, BSEL=1539, BSCALE=-7, CLK2X=0
#define BAUDCTRL_19200_4MHZ   0b1001011000000011
// 28800 Baudios, 4 Mhz, BSEL=983, BSCALE=-7, CLK2X=0
#define BAUDCTRL_28800_4MHZ   0b1001001111010111
// 38400 Baudios, 4 Mhz, BSEL=705, BSCALE=-7, CLK2X=0
#define BAUDCTRL_38400_4MHZ   0b1001001011000001
// 57600 Baudios, 4 Mhz, BSEL=428, BSCALE=-7, CLK2X=0
#define BAUDCTRL_57600_4MHZ   0b1001000110101100

// 9600 Baudios, 8 Mhz, BSEL=3269, BSCALE=-6, CLK2X=0
#define BAUDCTRL_9600_8MHZ    0b1010110011000101
// 19200 Baudios, 8 Mhz, BSEL=3205, BSCALE=-7, CLK2X=0
#define BAUDCTRL_19200_8MHZ   0b1001110010000101
// 28800 Baudios, Mhz, BSEL= , BSCALE= , CLK2X=
// 38400 Baudios, 8 Mhz, BSEL=1539, BSCALE=-7, CLK2X=0
#define BAUDCTRL_38400_8MHZ   0b1001011000000011
// 57600 Baudios, 8 Mhz, BSEL=983, BSCALE=-7, CLK2x=0
#define BAUDCTRL_57600_8MHZ   0b1001001111010111


/* BITS de control para interrupciones */
#define RREN          7
#define IVSEL         6

static volatile USART_t* serie;
static volatile USART_t* serie2;

void usart2Init(volatile USART_t* serialPort, volatile PORT_t* pinPort, baudSpeed baudrate, freqOsc_t freq)
{
	serie2 = serialPort;
	Init(serialPort, pinPort, baudrate, freq);
}

void usartInit(volatile USART_t* serialPort, volatile PORT_t* pinPort, baudSpeed baudrate, freqOsc_t freq)
{
	serie = serialPort;
	Init(serialPort, pinPort, baudrate, freq);
}

void Init(volatile USART_t* serialPort, volatile PORT_t* pinPort, baudSpeed baudrate, freqOsc_t freq)
{
	//pinPort->DIRSET = PIN3_bm;
	//pinPort->DIRCLR = PIN2_bm;

	pinPort->DIRSET = PIN7_bm;
    pinPort->DIRCLR = PIN6_bm;

	//serie = serialPort;
	serialPort->CTRLC = (ASYNCHRONOUS | DISABLED | BIT1S | BIT8);

	if(freq == f2mhz)
	{
		if(baudrate == B9600)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_9600_2MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_9600_2MHZ >> 8);
		}
		else if(baudrate == B19200)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_19200_2MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_19200_2MHZ >> 8);
		}
		else if(baudrate == B38400)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_38400_2MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_38400_2MHZ >> 8);
		}
		else if(baudrate == B57600)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_57600_2MHZ);
			serialPort->BAUDCTRLB = (BAUDCTRL_57600_2MHZ >> 8);
		}
	}
	else if(freq == f4mhz)
	{
		if(baudrate == B9600)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_9600_4MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_9600_4MHZ >> 8);
		}
		else if(baudrate == B19200)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_19200_4MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_19200_4MHZ >> 8);
		}
		else if(baudrate == B28800)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_28800_4MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_28800_4MHZ >> 8);
		}
		else if(baudrate == B38400)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_38400_4MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_38400_4MHZ >> 8);
		}
		else if(baudrate == B57600)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_57600_4MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_57600_4MHZ >> 8);
		}
	}
	else if(freq == f8mhz)
	{
		if(baudrate == B9600)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_9600_8MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_9600_8MHZ >> 8);
		}
		else if(baudrate == B19200)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_19200_8MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_19200_8MHZ >> 8);
		}
		else if(baudrate == B38400)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_38400_8MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_38400_8MHZ >> 8);
		}
		else if(baudrate == B57600)
		{
			serialPort->BAUDCTRLA = (unsigned char)(BAUDCTRL_57600_8MHZ);
			serialPort->BAUDCTRLB = (unsigned char)(BAUDCTRL_57600_8MHZ >> 8);
		}
	}

	serialPort->CTRLB = ((1 << RXEN) | (1 << TXEN) | (0 << CLK2X) | (0 << MPCM));
}

void usartTransmit(unsigned char data)
{
    while( (unsigned char volatile )(serie->STATUS & (1 << DREIF)) == 0 )
    	;
	serie->DATA = data;
}

void usart2Transmit(unsigned char data)
{
	while( (unsigned char volatile )(serie2->STATUS & (1 << DREIF)) == 0)
		;
	serie2->DATA = data;
}

unsigned char usartReceive(void)
{
	while( (unsigned char volatile)(serie->STATUS & (1 << RXC1)) == 0)
		;
	return serie->DATA; 
}

unsigned char usart2Receive(void)
{
	while( (unsigned char volatile)(serie2->STATUS & (1 << RXC1)) == 0)
		;
	return serie2->DATA;
}


void usartEnableRXInterrupt(void)
{
	serie->CTRLA |= ENABLE_RX_INTERRUPT;
	PMIC.CTRL |= PMIC_MEDLVLEN_bm;//PMIC_LOLVLEN_bm; 
}

void usart2EnableRXInterrupt(void)
{
	serie2->CTRLA |= ENABLE_RX_INTERRUPT;
	PMIC.CTRL |= PMIC_MEDLVLEN_bm;
}

void usartEnableTXInterrupt(void)
{
	serie->CTRLA |= ENABLE_TX_INTERRUPT;
	PMIC.CTRL |= PMIC_MEDLVLEN_bm; //PMIC_LOLVLEN_bm;
}

void usart2EnableTXInterrupt(void)
{
	serie2->CTRLA |= ENABLE_TX_INTERRUPT;
	PMIC.CTRL |= PMIC_MEDLVLEN_bm;
}