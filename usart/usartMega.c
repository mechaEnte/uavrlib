#include <usartMega.h>

const unsigned int UBRR_2400B_1MHZ = 51;
const unsigned int UBRR_4800B_1MHZ = 25;
const unsigned int UBRR_9600B_1MHZ = 12;

const unsigned int UBRR_9600B_2MHZ = 12;
const unsigned int UBRR_19200B_2MHZ = 6;
const unsigned int UBRR_38400B_2MHZ = 2;
const unsigned int UBRR_57600B_2MHZ = 1;

const unsigned int UBRR_9600B_4MHZ = 25;
const unsigned int UBRR_19200B_4MHZ = 12;
const unsigned int UBRR_38400B_4MHZ = 6;
const unsigned int UBRR_57600B_4MHZ = 3;

const unsigned int UBRR_9600B_8MHZ = 51;
const unsigned int UBRR_19200B_8MHZ = 25;
const unsigned int UBRR_38400B_8MHZ = 12;
const unsigned int UBRR_57600B_8MHZ = 8;

const unsigned int UBRR_2400_3_6864MHZ = 95;
const unsigned int UBRR_4800_3_6864MHZ = 47;
const unsigned int UBRR_9600_3_6864MHZ = 23;
const unsigned int UBRR_14400_3_6864MHZ = 15;
const unsigned int UBRR_19200_3_6864MHZ = 11;
const unsigned int UBRR_28800_3_6864MHZ = 7;
const unsigned int UBRR_38400_3_6864MHZ = 5;
const unsigned int UBRR_57600_3_6864MHZ = 3;
const unsigned int UBRR_76800_3_6864MHZ = 2;
const unsigned int UBRR_115200_3_6864MHZ = 1;
const unsigned int UBRR_230400_3_6864MHZ = 0;



void usartInit( baudSpeed baudrate, freqOsc_t freq )
{
	if( freq == f1mhz )
	{
		if( baudrate == B2400 )
		{
			UBRR0 = UBRR_2400B_1MHZ;
		}
		else if(baudrate == B4800 )
		{
			UBRR0 = UBRR_4800B_1MHZ;
		}
		else if(baudrate == B9600 )
		{
			UBRR0 = UBRR_9600B_1MHZ;
		}
		UCSR0A |= (1 << U2X0);
	}
	else if( freq == f2mhz )
	{
		if( baudrate == B9600 )
		{
			UBRR0 = UBRR_9600B_2MHZ;
		}
		else if( baudrate == B19200 )
		{
			UBRR0 = UBRR_19200B_2MHZ;
		}
		else if( baudrate == B38400 )
		{
			UBRR0 = UBRR_38400B_2MHZ;
		}
		else if( baudrate == B57600 )
		{
			UBRR0 = UBRR_57600B_2MHZ;
		}
	}
	else if( freq == f3_6864mhz)
	{
		if( baudrate == B2400 )
		{
			UBRR0 = UBRR_2400_3_6864MHZ;
		}
		else if( baudrate == B4800 )
		{
			UBRR0 = UBRR_4800_3_6864MHZ;
		}
		else if( baudrate == B9600 )
		{
			UBRR0 = UBRR_9600_3_6864MHZ;
		}
		else if( baudrate == B14400 )
		{
			UBRR0 = UBRR_14400_3_6864MHZ;
		}
		else if( baudrate == B19200 )
		{
			UBRR0 = UBRR_19200_3_6864MHZ;
		}
		else if( baudrate == B28800 )
		{
			UBRR0 = UBRR_28800_3_6864MHZ;
		}
		else if( baudrate == B38400 )
		{
			UBRR0 = UBRR_38400_3_6864MHZ;
		}
		else if( baudrate == B57600 )
		{
			UBRR0 = UBRR_57600_3_6864MHZ;
		}
		else if( baudrate == B76800 )
		{
			UBRR0 = UBRR_76800_3_6864MHZ;
		}
		else if( baudrate == B115200 )
		{
			UBRR0 = UBRR_115200_3_6864MHZ;
		}
		else if( baudrate == B230400 )
		{
			UBRR0 = UBRR_230400_3_6864MHZ;
		}
	}
	else if( freq == f4mhz )
	{
		if( baudrate == B9600 )
		{
			UBRR0 = UBRR_9600B_4MHZ;
		}
		else if( baudrate == B19200 )
		{
			UBRR0 = UBRR_19200B_4MHZ;
		}
		else if( baudrate == B38400 )
		{
			UBRR0 = UBRR_38400B_4MHZ;
		}
		else if( baudrate == B57600 )
		{
			UBRR0 = UBRR_57600B_4MHZ;
		}
	}

	else if( freq == f8mhz )
	{
		if( baudrate == B9600 )
		{
			UBRR0 = UBRR_9600B_8MHZ;
		}
		else if( baudrate == B19200 )
		{
			UBRR0 = UBRR_19200B_8MHZ;
		}
		else if( baudrate == B38400 )
		{
			UBRR0 = UBRR_38400B_8MHZ;
		}
		else if( baudrate == B57600 )
		{
			UBRR0 = UBRR_57600B_8MHZ;
		}
	}

	UCSR0B = ( ( 1 << RXEN0 ) | (1 << TXEN0) );
	UCSR0C = ( ( 0 << USBS0 ) | ( 1 << UCSZ00 ) | ( 1 << UCSZ01 )); // | ( 0 << UMSEL00 ) | ( 0 << UMSEL01 ) );

}

void usartTransmit( unsigned char data )
{
	while( !( UCSR0A & ( 1 << UDRE0 ) ) )
		;

	UDR0 = data;
}

unsigned char usartReceive( void )
{
	while( !( UCSR0A & ( 1 << RXC0 ) ) )
		;
	
	return UDR0;
}

void usartEnableInterrupt(void)
{
	UCSR0B |= (1 << RXCIE0);
}