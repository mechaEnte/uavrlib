#include <avr/io.h>
#include <avr/interrupt.h>
#include <freqatx.h>

#ifndef _USARTATXMEGA_H
#define _USARTATXMEGA_H

typedef enum
{
	B2400,
	B9600,
	B14400,
	B19200,
	B28800,
	B38400,
	B57600,
	B76800,
	B115200,
	B230400
} baudSpeed;

void usartInit(volatile USART_t* serialPort, volatile PORT_t* pinPort, baudSpeed baudrate, freqOsc_t freq);
void usartTransmit(unsigned char data);
unsigned char usartReceive(void); 
void usartEnableRXInterrupt(void);
void usartEnableTXInterrupt(void);

void usart2Init(volatile USART_t* serialPort, volatile PORT_t* pinPort, baudSpeed baudrate, freqOsc_t freq);
void usart2Transmit(unsigned char data);
unsigned char usart2Receive(void);
void usart2EnableRXInterrupt(void);
void usart2EnableTXInterrupt(void);

void Init(volatile USART_t* serialPort, volatile PORT_t* pinPort, baudSpeed baudrate, freqOsc_t freq);
#endif