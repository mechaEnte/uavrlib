#include <avr/io.h>
#include <avr/interrupt.h>
#include <freqmega.h>

#ifndef _USARTMEGA_H  
#define _USARTMEGA_H

typedef enum
{
	B2400,
	B4800,
	B9600,
	B14400,
	B19200,
	B28800,
	B38400,
	B57600,
	B76800,
	B115200,
	B230400
} baudSpeed;

void usartInit( baudSpeed baudrate, freqOsc_t freq );
void usartTransmit( unsigned char data );
unsigned char usartReceive( void );
void usartEnableInterrupt(void);

#endif