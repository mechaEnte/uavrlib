#ifndef _ADCDRIVER_H
#define _ADCDRIVER_H

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stddef.h>

void adc_init(ADC_t* adc);
unsigned char ReadCalibrationByte(unsigned char index);

#endif