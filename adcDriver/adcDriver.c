#include <adcDriver.h>

unsigned char ReadCalibrationBytes(unsigned char index)
{
	unsigned char result;
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	result = pgm_read_byte(index);

	NVM_CMD = NVM_CMD_NO_OPERATION_gc;

	return result;
}

void adc_init(ADC_t* adc)
{
	PR.PRPA &= ~0x02;

	// if(adc == ADCA)
	// {
		adc->CALL = ReadCalibrationBytes( offsetof(NVM_PROD_SIGNATURES_t, ADCACAL0));
		adc->CALL = ReadCalibrationBytes( offsetof(NVM_PROD_SIGNATURES_t, ADCACAL1));
		PORTA.DIR = 0x00;
	// }
	// else if(adc == ADCB)
	// {
		// adc->CALL = ReadCalibrationBytes(offsetof(NVM_PROD_SIGNATURES_t, ADCBCAL0));
		// adc->CALL = ReadCalibrationBytes(offsetof(NVM_PROD_SIGNATURES_t, ADCBCAL1));
		// PORTB.DIR = 0x00;
	// }

	adc->CTRLA |= ADC_ENABLE_bm;
	adc->CTRLB = ADC_RESOLUTION_12BIT_gc; //ADC_RESOLUTION_12BIT_gc;
	adc->REFCTRL = ADC_REFSEL_INT1V_gc;
	adc->PRESCALER = ADC_PRESCALER_DIV512_gc;
	adc->CH0.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	adc->CH0.MUXCTRL = ADC_CH_MUXPOS_PIN0_gc;

}
