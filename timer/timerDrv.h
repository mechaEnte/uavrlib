#ifndef _TIMER_H_
#define _TIMER_H_

#define F_CPU   4000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

void timerEnable(void);

#endif