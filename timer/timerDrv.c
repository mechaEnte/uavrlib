#include <timerDrv.h>

void timerEnable(void)
{
	TCD0.INTCTRLB = TC_CCAINTLVL_LO_gc;
	TCD0.PER = F_CPU/500;
	TCD0.CTRLB = TC0_CCAEN_bm;
	TCD0.CTRLA = TC_CLKSEL_DIV1_gc;
	PMIC.CTRL |= PMIC_LOLVLEN_bm;
}