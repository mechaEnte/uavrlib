#ifndef _FREQATX_H
#define _FREQATX_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <clock_t.h>

#define AVR_ENTER_CRITICAL_REGION( ) uint8_t volatile saved_sreg = SREG; \
                                     cli();

#define AVR_LEAVE_CRITICAL_REGION( ) SREG = saved_sreg;

void initSystem(freqOsc_t freq);

#endif