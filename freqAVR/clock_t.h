#ifndef _CLOCK_T_H
#define _CLOCK_T_H

typedef enum freqOsc
{
	f32mhz,
	f16mhz,
	f8mhz,
	f4mhz,
	f2mhz,
	f1mhz,
	f3_6864mhz,
	extOsc,
} freqOsc_t;

#endif