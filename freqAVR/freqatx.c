#include "freqatx.h"

void CCPWrite( volatile uint8_t * address, uint8_t value )
{
#ifdef __ICCAVR__

	// Store global interrupt setting in scratch register and disable interrupts.
        asm("in  R1, 0x3F \n"
	    "cli"
	    );

	// Move destination address pointer to Z pointer registers.
	asm("movw r30, r16");
#ifdef RAMPZ
	asm("ldi  R16, 0 \n"
            "out  0x3B, R16"
	    );

#endif
	asm("ldi  r16,  0xD8 \n"
	    "out  0x34, r16  \n"
#if (__MEMORY_MODEL__ == 1)
	    "st     Z,  r17  \n");
#elif (__MEMORY_MODEL__ == 2)
	    "st     Z,  r18  \n");
#else /* (__MEMORY_MODEL__ == 3) || (__MEMORY_MODEL__ == 5) */
	    "st     Z,  r19  \n");
#endif /* __MEMORY_MODEL__ */

	// Restore global interrupt setting from scratch register.
        asm("out  0x3F, R1");

#elif defined __GNUC__
	AVR_ENTER_CRITICAL_REGION( );
	volatile uint8_t * tmpAddr = address;
#ifdef RAMPZ
	RAMPZ = 0;
#endif
	asm volatile(
		"movw r30,  %0"	      "\n\t"
		"ldi  r16,  %2"	      "\n\t"
		"out   %3, r16"	      "\n\t"
		"st     Z,  %1"       "\n\t"
		:
		: "r" (tmpAddr), "r" (value), "M" (CCP_IOREG_gc), "i" (&CCP)
		: "r16", "r30", "r31"
		);

	AVR_LEAVE_CRITICAL_REGION( );
#endif
}

/*funcion init_system */
void initSystem(freqOsc_t freq)
{
	uint8_t PSconfig = 0;
	uint8_t clkCtrl;

	if(freq == extOsc){
		PSconfig = (uint8_t) CLK_PSADIV_1_gc | CLK_PSBCDIV_1_1_gc;
		OSC.CTRL=OSC_XOSCEN_bm;
		CCPWrite(&CLK.PSCTRL, PSconfig);
		/* esperar hasta que el oscilador esté listo */
		while((OSC.STATUS & OSC_XOSCRDY_bm) == 0);
   		CCP=0xD8;
   		CLK.CTRL=0x03; 
	}
	else {
		if(freq == f2mhz)
			/* configurar oscilador y fuente de reloj*/
			PSconfig = (uint8_t) CLK_PSADIV_8_gc | CLK_PSBCDIV_1_2_gc;    //2 MHZ

		else if(freq == f4mhz)
			/* configurar oscilador y fuente de reloj*/
			PSconfig = (uint8_t) CLK_PSADIV_4_gc | CLK_PSBCDIV_1_2_gc;    //4 MHZ

		else if(freq == f8mhz)
			/* configurar oscilador y fuente de reloj*/
			PSconfig = (uint8_t) CLK_PSADIV_4_gc | CLK_PSBCDIV_1_1_gc;    //8 MHZ		

		else if(freq == f16mhz)
			/* configurar oscilador y fuente de reloj*/
			PSconfig = (uint8_t) CLK_PSADIV_2_gc | CLK_PSBCDIV_1_1_gc;    //16 MHZ

		else if(freq == f32mhz)
			/* configurar oscilador y fuente de reloj*/
			PSconfig = (uint8_t) CLK_PSADIV_1_gc | CLK_PSBCDIV_1_1_gc;    //32 MHZ

		/* activar oscilador interno 32 mhz*/
		OSC.CTRL |= OSC_RC32MEN_bm;
		CCPWrite(&CLK.PSCTRL, PSconfig);
	
		/* esperar hasta que el oscilador esté listo */
		while((OSC.STATUS & OSC_RC32MRDY_bm) == 0);
	
		/* colocar el oscilador de anillo 32 mhz como la funte principal de reloj*/
		clkCtrl = ( CLK.CTRL & ~CLK_SCLKSEL_gm) | CLK_SCLKSEL_RC32M_gc;
		CCPWrite(&CLK.CTRL, clkCtrl);
	}	
	
}