#include <avr/io.h>

#ifndef _FREQMEGA_H
#define _FREQMEGA_H
#include <clock_t.h>

void initSystem( freqOsc_t freq );

#endif