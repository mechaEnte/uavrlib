#include <freqmega.h>

void initSystem( freqOsc_t freq )
{
	if( freq == f8mhz )
   {
      /* run at 8MHz */
      asm volatile( "ldi r16,0x80\n"
                     "sts 0x61, r16\n"
                     "ldi r16, 0x00\n"
                     "sts 0x61, r16\n"
                     "nop\n"
                     "nop\n"
                     "nop\n"
                     "nop\n"
                     "nop\n");
   }

   else if( freq == f4mhz )
   {
      /* run at 4MHz */
      asm volatile( "ldi r16,0x80\n"
                    "sts 0x61,r16\n"
                    "ldi r16,0x01\n"
                    "sts 0x61,r16\n"
                    "nop\n"
                    "nop\n"
                    "nop\n"
                    "nop\n"
                    "nop\n" );
   }

   else if( freq == f2mhz )
   {
      /* run at 2MHz */
      asm volatile( "ldi r16,0x80\n"
                    "sts 0x61,r16\n"
                    "ldi r16,0x02\n"
                    "sts 0x61,r16\n"
                    "nop\n" 
                    "nop\n" 
                    "nop\n" 
                    "nop\n" 
                    "nop\n" );
   }

   /* Disable the JTAG Pins */
   //MCUCR |= (1u << JTD);
   //MCUCR |= (1u << JTD);
   /*asm("ldi r16,0x80");
   asm("sts 0x55,r16");
   asm("ldi r16,0x80");
   asm("sts 0x55,r16");

   asm("nop");
   asm("nop");*/ 
}