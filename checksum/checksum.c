#include <checksum.h>

unsigned char checksum( const unsigned char* data, unsigned char size )
{
	unsigned char result = 0;
	for(int i=0; i < size; i++)
	{
		result += *(data + i);
	}

	result = ~result;
	result += 0x01;

	return result;
}