#ifndef _CHECKSUM_H
#define _CHECKSUM_H

/*
Calcula la suma de verificación de cada byte en una transmisión para 
puerto serie.
*/

unsigned char checksum( const unsigned char* data, unsigned char size );

#endif