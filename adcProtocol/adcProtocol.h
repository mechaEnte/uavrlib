#ifndef _ADCPROTOCOL_H
#define _ADCPROTOCOL_H

#include <checksum.h>

#define HEAD_PACKET         0xFA
#define COMMAND_PACKET      0xAD
#define POS_HEADER          0
#define POS_COMMAND_ADC     2
#define POS_SIZE_PACKET     1
#define POS_CHECKSUM        3	

#define LOW_PACKETSIZE       1
#define HIGH_PACKETSIZE      2
#define POS_FINAL_PACKETADC  3
#define LOW_SAMPLES          4
#define HIGH_SAMPLES         5
#define FIXED_SIZEBYTES_PACKET   6

unsigned char ADCprotocol(const unsigned char* dataPacket, const unsigned char* dataSamples,
	unsigned int samples, unsigned char* packetTransmitADC);

#endif