#include <adcProtocol.h>

unsigned char ADCprotocol(const unsigned char* dataPacket, const unsigned char* dataSamples, 
	                      unsigned int samples, unsigned char* packetTransmitADC)
{
	if((*(dataPacket+POS_HEADER) == HEAD_PACKET) &&
		(*(dataPacket+POS_COMMAND_ADC) == COMMAND_PACKET) &&
		(checksum(dataPacket, *(dataPacket+POS_SIZE_PACKET)) == *(dataPacket+POS_CHECKSUM)))
	{

		unsigned int packetSize = (samples * 2)+FIXED_SIZEBYTES_PACKET;

		*packetTransmitADC = HEAD_PACKET;
		*(packetTransmitADC+LOW_PACKETSIZE) = (unsigned char)(packetSize);
		*(packetTransmitADC+HIGH_PACKETSIZE) = (unsigned char)(packetSize >> 8);
		*(packetTransmitADC+POS_FINAL_PACKETADC) = COMMAND_PACKET;
		*(packetTransmitADC+LOW_SAMPLES) = (unsigned char)(samples);
		*(packetTransmitADC+HIGH_SAMPLES) = (unsigned char)(samples >> 8);

		for(unsigned int i=FIXED_SIZEBYTES_PACKET; i < (samples*2)+FIXED_SIZEBYTES_PACKET; i++)
		{
			*(packetTransmitADC+i) = *(dataSamples+(i-FIXED_SIZEBYTES_PACKET));
		}

		*(packetTransmitADC+packetSize) = checksum(packetTransmitADC, (unsigned char)(packetSize));

		return packetSize;
	}
	else
		return 0;
}