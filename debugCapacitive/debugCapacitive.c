#include <debugCapacitive.h>

volatile unsigned char keysEnable;
volatile qt_touch_lib_measure_data_t *qtMeasureData;

volatile unsigned char bufferTra[SIZETRABUFFER];
volatile unsigned char bufferRcv[SIZERCVBUFFER];
volatile unsigned char idxRcv = 0;

volatile unsigned char statusKey = 0;

volatile statusPack_t readySenData = packNo;
volatile statusPack_t stateTX = packNo;
volatile unsigned char positionTx=0;
volatile unsigned char sizeTx=0;
volatile USART_t *serial;
volatile p_delta = 0;

void debugInit(baudSpeed baudRate, freqOsc_t freq, volatile PORT_t* port, volatile USART_t* serialPort, qt_touch_lib_measure_data_t *qtData, unsigned char numKeyEnable)
{
	qtMeasureData = qtData;
	keysEnable = numKeyEnable;
	serial = serialPort;
	usartInit(serialPort, port, baudRate, freq);
	usartEnableRXInterrupt();
	usartEnableTXInterrupt();
}

void debugCalibrate(void)
{
	unsigned char poschksum = 0;
	qt_calibrate_sensing();
	bufferTra[poschksum] = HEADERPKG;
	poschksum++;
	bufferTra[poschksum] = IDENCAP;
	poschksum++;
	bufferTra[poschksum] = IDENCALIBRATE;
	poschksum++;
	bufferTra[poschksum] = checksum(bufferTra, poschksum);
	poschksum++;
	//for(unsigned char i=0; i < poschksum; i++)
		//usartTransmit(bufferTra[i]);
	sizeTx = poschksum;
	positionTx = 0;
	stateTX = packReady;
	serial->DATA = bufferTra[positionTx];
}

void debugNumKeys(void)
{
	unsigned char poschksum = 0;
	bufferTra[poschksum] = HEADERPKG;
	poschksum++;
	bufferTra[poschksum] = IDENCAP;
	poschksum++;
	bufferTra[poschksum] = IDENNUMKEYS;
	poschksum++;
	bufferTra[poschksum] = keysEnable;
	poschksum++;
	bufferTra[poschksum] = checksum(bufferTra, poschksum);
	poschksum++;
	//for(unsigned char i=0; i < poschksum; i++)
	//	usartTransmit(bufferTra[i]);
	sizeTx = poschksum;
	positionTx = 0;
	stateTX = packReady;
	serial->DATA = bufferTra[positionTx];
}

void debugStatusKeys(unsigned char key)
{
	unsigned char numPack = 0;
	unsigned char poschksum = 0;
	uint16_t delta, baseline, rawcount;
	rawcount = qtMeasureData->channel_signals[key];
	baseline = qtMeasureData->channel_references[key];
	delta = qt_get_sensor_delta(key);
	if(delta > 60000)
		delta = p_delta;
	p_delta = delta;
	bufferTra[poschksum] = HEADERPKG;
	poschksum++;
	bufferTra[poschksum] = IDENCAP;
	poschksum++;
	bufferTra[poschksum] = key;
	poschksum++;
	bufferTra[poschksum] = (unsigned char)(rawcount/256);
	poschksum++;
	bufferTra[poschksum] = (unsigned char)(rawcount%256);
	poschksum++;
	bufferTra[poschksum] = (unsigned char)(baseline/256);
	poschksum++;
	bufferTra[poschksum] = (unsigned char)(baseline%256);
	poschksum++;
	bufferTra[poschksum] = (unsigned char)(delta/256);
	poschksum++;
	bufferTra[poschksum] = (unsigned char)(delta%256);
	poschksum++;
	numPack = (keysEnable/8);
	if(keysEnable%8)
		numPack++;
	for(unsigned char i=0; i < numPack; i++)
	{
		bufferTra[poschksum] = qtMeasureData->qt_touch_status.sensor_states[i];
		poschksum++;
	}
	bufferTra[poschksum] = checksum(bufferTra, poschksum);
	poschksum++;
	//for(unsigned char i=0; i < poschksum; i++)
	//	usartTransmit(bufferTra[i]);
	sizeTx = poschksum;
	positionTx = 0;
	stateTX = packReady;
	serial->DATA = bufferTra[positionTx];
}

void debugUpdateData(void)
{
	if(readySenData == packReady)
	{
		readySenData = packNo;
		debugStatusKeys(statusKey);
	}
}

ISR(usart_vect)
{
	unsigned char dataRcv = 0;
	dataRcv = usartReceive();

	if(idxRcv < SIZERCVBUFFER)
	{
		bufferRcv[idxRcv] = dataRcv;
		idxRcv++;
	}
	else
	{
		for(unsigned char i = 0; i < (SIZERCVBUFFER-1); i++)
		{
			bufferRcv[i] = bufferRcv[i+1];
		}
		bufferRcv[idxRcv-1] = dataRcv;
	}

	if(idxRcv >= MINSIZEPKG)
	{
		if((bufferRcv[POSHEADERPKG] == HEADERPKG) && (bufferRcv[POSTYPEPKG] == IDENCAP))
		{	
			unsigned char deletebytes = 0;
			if((bufferRcv[POSIDEN] == IDENCALIBRATE) && (bufferRcv[POSCHK3] == checksum(bufferRcv, POSCHK3)))
			{
				debugCalibrate();
				deletebytes = 4;
			}
			else if((bufferRcv[POSIDEN] == IDENNUMKEYS) && (bufferRcv[POSCHK3] == checksum(bufferRcv, POSCHK3)))
			{
				debugNumKeys();
				deletebytes = 4;
			}
			else if((bufferRcv[POSIDEN] == IDENSTATUSKEY) && (bufferRcv[POSCHK4] == checksum(bufferRcv, POSCHK4)))
			{
				//debugStatusKeys(bufferRcv[POSKEYSTATUS]);
				statusKey = bufferRcv[POSKEYSTATUS];
				readySenData = packReady;
				deletebytes = 5;
			}


			if(deletebytes > 0)
			{
				for(unsigned char i=0; i < SIZERCVBUFFER; i++)
					bufferRcv[i] = 0x00;
				idxRcv = 0;
			}
		}
	}
}

ISR(usartTx_vect)
{
	if(stateTX == packReady)
	{
		positionTx++;
		if(positionTx == sizeTx)
		{
			stateTX == packNo;
		}
		else
		{
			serial->DATA = bufferTra[positionTx];
		}
	}
}