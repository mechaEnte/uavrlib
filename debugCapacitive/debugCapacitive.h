#ifndef _DEBUGCAPACITIVE_H
#define _DEBUGCAPACITIVE_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <checksum.h>
#include <usartAtxMega.h>
#include "touch_api.h"

#define SIZERCVBUFFER    25
#define SIZETRABUFFER    15
#define HEADERPKG        0xFA
#define IDENCAP          0xCA
#define IDENCALIBRATE    0xFE
#define IDENNUMKEYS      0xBA
#define IDENSTATUSKEY    0xBC

#define POSHEADERPKG     0
#define POSTYPEPKG       1
#define POSIDEN          2
#define POSKEYSTATUS     3

#define MINSIZEPKG       4

#define POSCHK3          3
#define POSCHK4          4

typedef enum statusPack
{
	packReady,
	packNo,
}statusPack_t;

void debugInit(baudSpeed baudRate, freqOsc_t freq, volatile PORT_t* port, volatile USART_t* seriePort, qt_touch_lib_measure_data_t* qtData, unsigned char numKeyEnable);

void debugCalibrate(void);
void debugNumKeys(void);
void debugStatusKeys(unsigned char key);
void debugUpdateData(void);

#endif